let userNumber=Number(prompt("Provide a number: "));


if(!userNumber){
    console.warn("Number required.");
}

console.log("The number you provided is " + userNumber + ".");


for(let i = userNumber; i >= 0; i--){
    if(i <= 50){
        console.log("The current value is less than/equal to 50. Terminate loop.")
        break;
    } 

    else if(i % 10 == 0){
        console.log("The number is divisible by 10. Skipping the number.")
    } 

    else if(i % 5 == 0){
        console.log(i);
    }
}

let longWord = "supercalifragilisticexpialidocious";
let consonants = "";
for(let i=0; i<longWord.length; i++){
    if(longWord[i] == "a" || longWord[i] == "e" || longWord[i] == "i" || longWord[i] == "o" ||longWord[i] == "u"){
    }

    else{
       consonants+=longWord[i];
    }
}

console.log(longWord);
console.log(consonants);